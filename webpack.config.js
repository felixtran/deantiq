const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const glob = require('glob');

// https://webpack.js.org/configuration/
module.exports = {
  entry: {
    main: path.join(__dirname, '_webpack', 'main'),
    home: path.join(__dirname, '_webpack/pages', 'home'),
    about: path.join(__dirname, '_webpack/pages', 'about'),
    
  },
  output: {
    path: path.resolve(__dirname, 'assets/js'),
    filename: '[name].js',
  },
  resolve: {
    extensions: ['.json', '.js', '.jsx'],
    modules: ['node_modules'],
  },
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        sourceMap: true, // set to true if you want JS source maps
      }),
    ],
  },
  watchOptions: {
    poll: true,
    ignored: /node_modules/
  },
  module: {
    rules: [
      { test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader' },
      // {
      //   test: /\.scss$/,
      //   use: [
      //     {
      //       loader: 'css-loader',
      //       options: {
      //         importLoaders: 1, // https://webpack.js.org/loaders/postcss-loader/
      //       },
      //     },
      //     {
      //       loader: 'postcss-loader',
      //       options: {
      //         plugins: () => [
      //           require('cssnano')(), // https://cssnano.co/
      //         ],
      //       },
      //     },
      //     {
      //       loader: 'sass-loader',
      //       options: {},
      //     },
      //   ],
      // },
    ],
  },
  cache: false
};
