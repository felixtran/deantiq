import {gsap} from 'gsap';
import { EventEmitter } from 'events';


export class Loader extends EventEmitter {
  constructor(el) {
    super();
    this.DOM = {el};
    this.logo = this.DOM.el.querySelector('div');
    this.isAnimating = false;
  }

  hideLoader() {
    gsap.to(this.DOM, {
      translateY: '-100%',
      duration: 0.1,
      zIndex: '0',
    })
  }

  reveal() {
    console.log(this.DOM)
    console.log(this.logo)
    if ( this.isAnimating ) {
      return false;
    }
    this.isAnimating = true;
    gsap.timeline({
      onComplete: () => {
        this.isAnimating = false
      }
    })
      .set(this.DOM,{translateY: 0, zIndex: 99, opacity: 0, scale: 0.85})
      .add(gsap.to(this.logo,{
        ease: "power2",
        duration: 1,
        opacity: 1,
        scale: 1,
      }))
  }

  hide() {
    if ( this.isAnimating ) {
      return false;
    }
    this.isAnimating = true;
    gsap.timeline({
      onComplete: () => {
        this.isAnimating = false
        this.hideLoader();
      }
    })
      .add(gsap.to(this.DOM, {
        ease: "power2",
        duration: 1,
        opacity: 0,
        scale: 0.85,
        delay: 0.6,
      }))
  }
}
