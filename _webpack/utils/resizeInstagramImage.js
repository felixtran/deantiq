import { throttle, filter } from 'lodash';
import {gsap} from "gsap";

const commonSetting = {
  ease: "power2",
  duration: 0.45,
}

export const resizeInstagramImage = throttle(function (){
  let instagramImageItems = document.getElementsByClassName('instagramImage');
  let offsetWidth = instagramImageItems[0].offsetWidth;
  console.log('RESIZE',offsetWidth)
  filter(instagramImageItems,function (item) {
    gsap.to(item,{
      ...commonSetting,
      height: offsetWidth,
    })
  })
},400)
