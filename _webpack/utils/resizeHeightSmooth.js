import { throttle, filter } from 'lodash';
import {gsap} from "gsap";

const commonSetting = {
  ease: "power2",
  duration: 0.65,
}

export const resizeHeightSmooth = throttle(function (){
  let innerheight = window.innerHeight;
  let fullHeightItems = document.getElementsByClassName('full-height');
  filter(fullHeightItems,function (item) {
    gsap.to(item,{
      ...commonSetting,
      height: innerheight,
      delay: 1,
    })
  })
},1000)
