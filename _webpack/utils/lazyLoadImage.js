export function initLazyLoadImage() {
  let  options = {
    root: document.querySelector('.root'),
    rootMargin: '0px, 0px, 100px, 0px'
  };

  let  images = [...document.querySelectorAll('.targetImages')];

  const callback = (entries) => {

    entries.forEach(entry => {
      if (entry.isIntersecting) {
        observer.unObserve('entry.target');
      }
// handle other code logic here
    })
  }

  let observer = new intersectionObserver(options, callback);

  images.forEach(image => {
    observer.observe(image);
  })
}
