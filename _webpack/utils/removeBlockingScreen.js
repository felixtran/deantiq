export function removeBlockingScreen() {
  let homepage = document.getElementById('homepage')
  homepage.classList.remove("fixed");
  homepage.classList.remove("overflow-hidden");
}
