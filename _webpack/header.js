import {gsap} from 'gsap';
import {throttle} from 'lodash';

const commonSetting = {
  ease: "power2",
  duration: 0.65,
}

export function animateHeader() {
  let heroSectionHeight = document.getElementById('hero-section').offsetHeight;
  let headerBackground =  window.pageYOffset <= heroSectionHeight/2 ? 'transparent' : '#0A3944';
  if (window.pageYOffset < document.lastPageYOffSet || window.pageYOffset < heroSectionHeight/2){
    // downscroll code
    gsap.to("#home-header", {
      ...commonSetting,
      translateY: '0',
      background: headerBackground,
    })
  } else {
    // upscroll code
    gsap.to("#home-header", {
      ...commonSetting,
      translateY: '-100%',
      background: '#0A3944'
    })
  }
  document.lastPageYOffSet = window.pageYOffset;
}

export const withThrottle = throttle(function (){
  animateHeader()
},400)
