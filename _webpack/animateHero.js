import {gsap} from "gsap";
import {white} from "tailwindcss/colors";
import {removeBlockingScreen} from "./utils/removeBlockingScreen";

const READING_INTRO1_TIME = 3;

function removeElement(element) {
  if (typeof(element) === "string") {
    element = document.querySelector(element);
  }
  return function() {
    element.parentNode.removeChild(element);
  };
}

const commonSetting = {
  ease: "power2",
  duration: 1,
}

function animateFlower() {
  let timeline = gsap.timeline();
  // let logo1 = document.getElementById('logo1').attr("d")

  timeline.add(gsap.to('#logo1', 1, {morphSVG: {shape: '#logo2'}}));
  timeline.add(gsap.to('#logo1', 1, {morphSVG: {shape: '#logo3'}}));
  // timeline.add(gsap.to('#logo1',1, {morphSVG:logo1}));
  timeline.add(gsap.to(document.querySelector('.lg-preloader'),
    {
      ...commonSetting,
      opacity: 0,
      onComplete: () => {
      }
    }
  ));
}

function animateLoopFlower() {
  let timeline = gsap.timeline({repeat: -1});
  let logo1 = document.getElementById('logo1').getAttribute("d");
  console.log('logo1', logo1);

  timeline.add(gsap.to('#logo1', 1, {morphSVG: {shape: '#logo2'}}));
  timeline.add(gsap.to('#logo1', 1, {morphSVG: {shape: '#logo3'}}));
  timeline.add(gsap.to('#logo1',1, {morphSVG:logo1}));
}

export function initLoader() {

  let loaderTimline = gsap.timeline();
  let loaderLogo = document.getElementById('loader-logo');
  let loader = document.getElementById('loader');
  loaderTimline.add(gsap.to(loaderLogo,{
    ease: "power2",
    duration: 1,
    opacity: 1,
    scale: 1,
  }))
  loaderTimline.add(gsap.to(loader, {
    ease: "power2",
    duration: 1,
    opacity: 0,
    scale: 0.85,
    delay: 0.6,
    onComplete: () => {
      animateIntro();
      hideLoader();
      // removeLoader()
    }
  }))
  // gsap.to(
  //   document.querySelector('#loader-logo'),
  //   {
  //     ease: "power2",
  //     duration: 1,
  //     opacity: 0,
  //     onComplete: () => {
  //       animateIntro();
  //       removeLoader()
  //       done()
  //     }
  //   }
  // )
}



function hideLoader() {
  let loader = document.getElementById('loader');
  gsap.to(loader, {
    translateY: '-100%',
    duration: 0.1,
    zIndex: 0,
  })
}

function removeLoader() {
  let loader = document.getElementById('loader');
  loader.remove();
}

function revealText(el, duration, delay) {
  return gsap.to(el, {
    duration: duration ? duration : 1.5,
    delay: delay ? delay : 0.5,
    opacity:1,
    ease: "power2",
  })
}

function hideText(el, then) {
  return gsap.to(el, {
    duration:1.5,
    delay:0.5,
    opacity:0,
    ease: "power2",
    onComplete: () => {
      if(then) {
        then();
      }
    }
  })
}


function revealBackground() {



  gsap.to('#hero-video, #hero-overlay', {
    ...commonSetting,
    scale: 1,
    opacity: 1,
  })

  gsap.to('#hero-header-text', {
    ...commonSetting,
    scale: 1.1,
    color: white,
  })

  gsap.to('#home-header', {
    ...commonSetting,
    scale: 1,
    opacity: 1,
    delay: 0.2,
  })

  gsap.to('#hero-socials', {
    ...commonSetting,
    opacity: 1
  })

  // gsap.to('#noise', {
  //   ...commonSetting,
  //   opacity: 0.05,
  // })

  gsap.to('#hero-intro-wrapper', {
    ...commonSetting,
    opacity: 0,
  })

  // gsap.to('#hero-video, #hero-overlay', {
  //   ...commonSetting,
  //   scale: 1.3,
  //   duration: 70,
  // })
}

export function removeNoise() {
  let noise = document.getElementById('noise')
  noise.remove()
}


export function animateIntro() {

  let skipButton = document.getElementById('skip-button')

  skipButton.onclick = function () {
    timeline.paused();
    animateHero()
  }

  let timeline = gsap.timeline({
    onComplete: () => animateHero()
  });

  // timeline.add(hidePreloader);
  timeline.add(revealText('#hero-intro-1',null));
  // timeline.add(pauseText('#hero-intro-1',READING_INTRO1_TIME))
  timeline.add(hideText('#hero-intro-1',null),"+=2");
  timeline.add(revealText('#hero-intro-2'));
  timeline.add(revealText('#hero-intro-3'));
  timeline.add(hideText('#hero-intro-2, #hero-intro-3',null));
  timeline.play()
}

export function animateHero() {
  let hero = gsap.timeline();
  // REMOVE INTRO
  hero.add(gsap.to('#hero-intro-wrapper',{
    ...commonSetting,
    opacity: 0,
    duration: 1,
  }))
  // REVEAL HEADER
  hero.add(gsap.to('#hero-header-text',{
    ...commonSetting,
    opacity: 1,
    onComplete: () => {
      removeBlockingScreen();
      let intro = document.getElementById('hero-intro-wrapper')
      if(intro) {
        intro.remove();
      }
    }
  }));
  // REVEAL BACKGROUND
  hero.add(revealBackground);
  hero.play()
}

export function scrollToHideHero() {
  let tl = gsap.timeline();
  tl.to("#home-overlay", {
    opacity: 0,
  });
  tl.to("#hero-header-text", {
    opacity: 0,
  })
}
