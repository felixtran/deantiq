import luge from '@waaark/luge'
import {gsap} from 'gsap';
import scrollMonitor from 'scrollmonitor';
import {resizeInstagramImage} from "./utils/resizeInstagramImage";

// Get elements to watch
const socialSection = document.getElementById('social-section')
const dataImageRevealList = document.querySelectorAll('[data-image-reveal]');

// Init Watcher
const socialWatcher = scrollMonitor.create(socialSection,500);

// First Resize Image when scroll to social sections
socialWatcher.enterViewport(function () {
  resizeInstagramImage()
})


for (let dataImageRevealListElement of dataImageRevealList) {
  const elementWatcher = scrollMonitor.create( dataImageRevealListElement, -150 );

  elementWatcher.enterViewport(function() {
    console.log( 'I have entered the viewport',dataImageRevealListElement);
    revealImage(dataImageRevealListElement)
  });
  elementWatcher.exitViewport(function() {
    elementWatcher.destroy();
  });
}

function revealImage(el) {
  let animationSetting = {
    ease: "power2",
  }

  let overlay = el.getElementsByTagName('div');
  let image = el.getElementsByTagName('img');
  let animation = gsap.timeline({})
  animation.add(gsap.to(overlay, {
    opacity: 1,
    duration: 0.65,
    ease: 'power2'
  }))
  animation.add(gsap.to(overlay, {
    opacity: 0,
    translateY: '-100%',
    duration: 0.65,
    delay: 0.05,
    ease: 'power2'
  }))
  animation.add(gsap.to(image, {
    opacity: 1,
    scale: 1,
    duration: 0.75,
    ease: 'power2'
  }),0.7)

}

// Add a callback function
// luge.reveal.add('in', 'image-reveal', (element) => {
//   console.log('REVEAL', element)
//   // Add overlay
//   // Set opacicy of image to 0
//   // When image load completely then reveal image
//   // When the element gets in viewport
//   gsap.to(element, {
//
//   })
// })
