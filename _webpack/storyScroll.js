export function runAll() {

  let images = [...document.querySelectorAll('.galleryImage')];
  let slider = document.querySelector('.slider');
  let sliderContainer = document.querySelector('.slider-container');
  let sliderWidth;
  let imageWidth;
  let current = 0;
  let target = 0;
  let ease = 0.1;

  function lerp(start, end, t) {
    return start * (1-t) + end * t;
  }

  function setTransform(el, transform) {
    el.style.transform = transform
  }

  function init() {
    sliderWidth = slider.getBoundingClientRect().width;
    imageWidth = sliderWidth / images.length;
    // sliderContainer.style.height = `${sliderWidth}px`
  }

  function animate() {
    current = parseFloat(lerp(current, target, ease)).toFixed(2);
    target = window.screenY;
    console.log(current)
    setTransform(slider, `translateX(-$(current)px`)
    // requestAnimationFrame(animate);
  }

  init()
  animate()
  console.log(imageWidth)
  console.log(sliderContainer.style);
}
