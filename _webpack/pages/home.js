console.log('HOME PAGE')

import {initLoader} from "../animateHero";
import {withThrottle} from "../header";
import {resizeHeightSmooth} from "../utils/resizeHeightSmooth";
import {resizeInstagramImage} from "../utils/resizeInstagramImage";
import '../reveal';
import { initSlider } from "../slider";


// SET TRANSFORM
function setTransform(el, transform) {
  el.style.transform = transform
}



function checkVideoLoaded() {
  let video = document.getElementById("hero-video");
  if ( video.readyState === 4 ) {
    // it's loaded
    console.log('video loaded');
    video.play();
  }
}

function disableZoom() {
  document.addEventListener('gesturestart', function (e) {
    e.preventDefault();
  });
}

window.addEventListener('load', function () {

  // INIT LOADER
  initLoader();
initSlider();

  // ANIMATE HEADER
  window.onscroll = function() {withThrottle();};

  //HANDLE RESIZE
  resizeHeightSmooth();
  // FIRST RESIZE
  window.onresize = function () {
    resizeHeightSmooth();
    resizeInstagramImage();
  };

  disableZoom()
  checkVideoLoaded()

  // noise();
})
