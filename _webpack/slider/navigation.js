import { gsap } from 'gsap';
export class Navigation {
  constructor(el, slidesTotal) {
    this.DOM = {el};
    this.DOM.ctrls = {
      next: this.DOM.el.querySelector('.slides-nav__button--next'),
      prev: this.DOM.el.querySelector('.slides-nav__button--prev'),
      touchScreen: this.DOM.el
    };
    this.slidesTotal= slidesTotal;
    this.DOM.current = this.DOM.el.querySelector('.slides-nav__index-current');
    this.DOM.wrapIndex = this.DOM.current.parentNode;
    this.DOM.total = this.DOM.el.querySelector('.slides-nav__index-total');
    this.DOM.bookmark = this.DOM.el.querySelector('.slides-nav__location--text');
    this.DOM.bookmarkWrap = this.DOM.el.querySelector('.slides-nav__location');
    this.bookmark = JSON.parse(this.DOM.bookmarkWrap.getAttribute('data-bookmarks'));
    this.initWidthWrapBookmark = this.DOM.bookmarkWrap.offsetWidth;
    this.paddingLeftWrapBookmark = parseInt(window.getComputedStyle(this.DOM.bookmarkWrap.parentNode).getPropertyValue('padding-left'),10)
    this.init();
  }
  init(){
    this.DOM.bookmarkWrap.style.width = `${this.initWidthWrapBookmark + this.paddingLeftWrapBookmark + this.DOM.bookmark.offsetWidth}px`;
    this.DOM.current.innerHTML = 1;
  }
  // updates the current value
  updateCurrent(position, direction) {
    let upcomingIndex = position, prevIndex = 0;
    if ( direction === 'next' ) {
      prevIndex = position > 0 ? upcomingIndex - 1 : this.slidesTotal - 1;
    }
    else {
      prevIndex = position < this.slidesTotal-1 ? upcomingIndex + 1 : 0;
    }

    // Create new DOM in bookmark
    let newItem = document.createElement("span");
    let textNewItem = document.createTextNode(this.bookmark[upcomingIndex])
    let newItemAttributes = document.createAttribute("class");
    newItemAttributes.value = "slides-nav__location--text text-sm md:text-base";
    newItem.setAttributeNode(newItemAttributes);

    // Create new DOM INDEX
    let newItemIndex = document.createElement("span");
    let textItemIndex = document.createTextNode(upcomingIndex+1)
    let newItemIndexAttributes = document.createAttribute("class");
    newItemIndexAttributes.value = "text-white text-4xl md:text-6xl slides-nav__index-current absolute top-1/2 transform -translate-y-1/2 right-0";
    newItemIndex.setAttributeNode(newItemIndexAttributes);
    

    // Handle add DOM
    if(direction) {
      newItem.appendChild(textNewItem);
      newItemIndex.appendChild(textItemIndex)
      this.DOM.bookmarkWrap.insertBefore(newItem, this.DOM.bookmark);
      this.DOM.wrapIndex.insertBefore(newItemIndex, this.DOM.current);

      gsap
      .timeline({
        onComplete: () => {
          this.isAnimating = false;
        }
      })
      .addLabel('start', 0)
      .set(this.DOM.bookmark, {opacity: 1}, 'start')
      .set(newItem, {opacity: 0, y: '150%', rotate: 15}, 'start')
      .set(this.DOM.current, {opacity: 1, y: '0%', rotate: 0}, 'start')
      .set(newItemIndex, {y: '150%', rotate: 35}, 'start')
      .to(this.DOM.bookmarkWrap, {width:  this.initWidthWrapBookmark, duration: 0.9, ease: 'power3'},'start')
      .to(this.DOM.bookmark, {
        duration: 0.1,
        ease: 'power3.in',
        opacity: 0,
      }, 'start+=0.8')
      .to(this.DOM.current, {
        duration: 0.9,
        ease: 'power3.in',
        y: '150%',
        rotate: 35,
      }, 'start')
      .to(newItem, {
        duration: 0.8,
        ease: 'power3',
        opacity: 1,
        rotate: 0,
        y: '0%',
      }, 'start+=1')
      .to(newItemIndex, {
        duration: 0.9,
        ease: 'power3',
        opacity: 1,
        rotate: 0,
        y: '0%',
        onStart: () => {
          this.DOM.current = this.DOM.el.querySelectorAll('.slides-nav__index-current')[1];
        },
        onComplete: ()=> {
          this.DOM.current.remove();
          this.DOM.current = this.DOM.el.querySelector('.slides-nav__index-current');
        }
      }, 'start+=0.9')
      .to(this.DOM.bookmarkWrap, {
        duration: 0.9,
        ease: 'power3',
        width: this.initWidthWrapBookmark + this.paddingLeftWrapBookmark + this.DOM.el.querySelectorAll('.slides-nav__location--text')[0].offsetWidth,
        onStart: () => {
          this.DOM.bookmark = this.DOM.el.querySelectorAll('.slides-nav__location--text')[1];
        },
        onComplete: ()=> {
          this.DOM.bookmark.remove();
          this.DOM.bookmark = this.DOM.el.querySelector('.slides-nav__location--text');
        }
      }, 'start+=0.9')
    }
  }
}
