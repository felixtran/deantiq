export class Slide {
  constructor(el) {
    this.DOM = {el};
    this.DOM.headline = this.DOM.el.querySelector('.slides__caption-headline');
    this.DOM.text = this.DOM.headline.querySelectorAll('.text-row > span');
    this.DOM.CTAtext = this.DOM.headline.querySelectorAll('.slides__CTA-text > a');
    this.DOM.link = this.DOM.el.querySelector('.slides__caption-link');
  }
}
