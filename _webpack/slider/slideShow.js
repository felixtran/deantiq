import { Slide } from './slide';
import { EventEmitter } from 'events';
import { gsap } from 'gsap';

export class Slideshow extends EventEmitter {
  constructor(el) {
    super();
    this.DOM = {el};

    this.DOM.slides = [...this.DOM.el.querySelectorAll('.slide')];
    this.slides = [];
    this.DOM.slides.forEach(slide => this.slides.push(new Slide(slide)));
    this.slidesTotal = this.slides.length;
    this.current = 0;
    this.init();
  }
  init() {
    this.DOM.slides[this.current].classList.add('slide--current');
  }
  next() {
    this.navigate('next');
  }
  prev() {
    this.navigate('prev');
  }
  navigate(direction) {
    if ( this.isAnimating ) {
      return false;
    }
    this.isAnimating = true;

    const currentSlide = this.slides[this.current];
    // update current
    if ( direction === 'next' ) {
      this.current = this.current < this.slidesTotal-1 ? this.current+1 : 0;
    }
    else {
      this.current = this.current > 0 ? this.current-1 : this.slidesTotal-1;
    }
    const upcomingSlide = this.slides[this.current];

    gsap
      .timeline({
        onStart: () => upcomingSlide.DOM.el.classList.add('slide--current'),
        onComplete: () => {
          this.isAnimating = false;
          currentSlide.DOM.el.classList.remove('slide--current');
        }
      })
      .addLabel('start', 0)
      .set(upcomingSlide.DOM.el, {opacity: 1}, 'start')
      .set(upcomingSlide.DOM.text, {y:'150%', rotate: 15}, 'start')
      .set(upcomingSlide.DOM.CTAtext, {opacity: 0},'start')
      .set(upcomingSlide.DOM.link, {opacity: 0}, 'start')
      .to(currentSlide.DOM.text, {
        duration: 0.9,
        ease: 'power3.in',
        y: '150%',
        rotate: 5
        //stagger: 0.15
      }, 'start')
      .to(currentSlide.DOM.link, {
        duration: 0.5,
        ease: 'power3',
        opacity: 0
      }, 'start')
      .to(currentSlide.DOM.CTAtext, {
        duration: 0.5,
        ease: 'power3',
        opacity: 0
      }, 'start')
      .to(upcomingSlide.DOM.text, {
        duration: 0.9,
        ease: 'power3',
        y: '0%',
        rotate: 0,
        stagger: direction === 'next' ? 0.1 : -0.1
      }, 'start+=0.9')
      // animate the upcoming slide link in
      .to(upcomingSlide.DOM.link, {
        duration: 1,
        ease: 'power3.in',
        opacity: 1
      }, 'start+=0.6')
      .to(upcomingSlide.DOM.CTAtext, {
        duration: 1,
        ease: 'power3.in',
        opacity: 1,
      }, 'start+=0.8')
      if(direction == 'next') {
        this.emit('updateCurrent', this.current, 'next');
      } else {
        this.emit('updateCurrent', this.current, 'previous');
      }
    
  }
}
