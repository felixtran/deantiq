

// SLIDER
import {Navigation} from './navigation';
import {Slideshow} from './slideshow';
import {Sketch} from './sketch';
function addListenerMulti(el, s, fn) {
  s.split(' ').forEach(e => el.addEventListener(e, fn, false));
}

export function initSlider(){
  // SLIDER
  let sketch = new Sketch({
    duration: 1.8,
    // debug: true,
    uniforms: {
      intensity: {value: 1, type:'f', min:1., max:100}
    },
    fragment: `
      uniform float time;
      uniform float progress;
      uniform float intensity;
      uniform float width;
      uniform float scaleX;
      uniform float scaleY;
      uniform float transition;
      uniform float radius;
      uniform float swipe;
      uniform sampler2D texture1;
      uniform sampler2D texture2;
      uniform sampler2D displacement;
      uniform vec4 resolution;
      varying vec2 vUv;
      mat2 rotate(float a) {
        float s = sin(a);
        float c = cos(a);
        return mat2(c, -s, s, c);
      }
      const float PI = 3.1415;
      const float angle1 = PI *0.25;
      const float angle2 = -PI *0.75;
  
  
      void main()	{
        vec2 newUV = (vUv - vec2(0.5))*resolution.zw + vec2(0.5);
  
        vec2 uvDivided = fract(newUV*vec2(intensity,1.));
  
  
        vec2 uvDisplaced1 = newUV + rotate(3.1415926/4.)*uvDivided*progress*0.1;
        vec2 uvDisplaced2 = newUV + rotate(3.1415926/4.)*uvDivided*(1. - progress)*0.1;
  
        vec4 t1 = texture2D(texture1,uvDisplaced1);
        vec4 t2 = texture2D(texture2,uvDisplaced2);
  
        gl_FragColor = mix(t1, t2, progress);
  
      }
  
    `
  });

  const slideshow = new Slideshow(document.querySelector('.slideshow'));

  const navigation = new Navigation(document.querySelector('.slides-nav'),slideshow.slidesTotal);
  // navigation events
  navigation.DOM.ctrls.next.addEventListener('click', () => {
    slideshow.next();
    sketch.execute('next');
  });
  navigation.DOM.ctrls.prev.addEventListener('click', () => {
    slideshow.prev();
    sketch.execute('prev');
  });
  // navigation.DOM.ctrls.touchScreen.addEventListener('touchstart', (evs) => {
  //   if(evs.cancelable) evs.preventDefault();
  //   let touchStartX = evs.changedTouches[0].pageX;
  //   evs.target.addEventListener('touchend', (eve) => {
  //       if(eve.cancelable) eve.preventDefault();
        
  //       console.log('process_touch end cx', eve.changedTouches[0].pageX);
  //       let touchEndX = eve.changedTouches[0].pageX;

  //       if(touchStartX - touchEndX < -150) {
  //         console.log('prev: ', touchStartX - touchEndX < -150);
  //         console.log('prev 1: ', touchStartX - touchEndX);
  //         sketch.execute('prev');
  //         slideshow.prev();
        
  //       };
  //       if(touchStartX - touchEndX > 150) {
  //         console.log('next: ', touchStartX - touchEndX > 150);
  //         console.log('next 1: ', touchStartX - touchEndX);
  //         sketch.execute('next');
  //         slideshow.next();
  //       };
  //     });
  // });

  // set the initial navigation current slide value
  navigation.updateCurrent(slideshow.current);
  // set the navigation total number of slides
  navigation.DOM.total.innerHTML = slideshow.current < 10 ? `${slideshow.slidesTotal}` : slideshow.slidesTotal;
  // when a new slide is shown, update the navigation current slide value
  slideshow.on('updateCurrent', (position, direction) => navigation.updateCurrent(position, direction));


  
  sketch.current = slideshow.current;

};