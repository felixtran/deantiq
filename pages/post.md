---
id: story
layout: post-journey
title: Chuyện Ba Hoa
description: Quá chuyên với người thường, Quá thường với dân chuyên.
permalink: /hanh-trinh-template
---


Tất nhiên, chúng tôi sẽ bỏ qua phần mọi người lên kế hoạch và chuẩn bị. Nó chỉ là một chuyến đi và các bạn cũng chẳng cần phải biết chúng tôi đã soạn bao nhiêu cái quần xì hay phải hôn tạm biệt các cô bạn gái thêm bao nhiêu lần! (trừ Đạt)

Ngày khởi hành đã đến, chúng tôi tập trung ở nhà Huy và cùng đi Grab đến bến xe miền Tây. Thứ duy nhất vui vẻ và mang điều phước lành ở đoạn đầu có lẽ là cuộc trò chuyện với ba cô gái Tây xinh đẹp chung xe. Tôi đã có nhiều suy nghĩ khác nhau về việc tại sao nhà xe lại làm giường chỉ để cho một người nằm trong khi cả xã hội đang lên án sự vô cảm và xa cách của con người? Và nếu như tôi đọc số điện thoại của mình thay cho mật khẩu wifi thì tôi có thể khởi động làm nóng với cặp núi đôi của họ thay cho Tà Năng hay không? Chẳng ai biết cả. (Trừ Đạt không muốn leo)

Sau khi đã dặn anh lơ xe đến 8 lần rằng nhất định không được chạy thẳng đến Đà Lạt mà phải thả chúng cháu ở **ngã ba Tà Hine**, anh ấy ngao ngán và bảo được thôi, giờ thì chúng mày cần ngủ đi trước khi tao thả tất cả xuống ngã mẹ (trong một gia đình thì không thể chỉ có ba mới ngã được).

Sau tròn 6 tiếng chạy xe bòn bon, anh lơ xe vui vẻ đá đít bọn tôi xuống địa điểm. Mặc dù đã lường trước về độ lạnh, nhưng tôi vẫn không tránh khỏi việc bị sốc nhiệt và lầm bầm các thể loại rằng lũ thất tình là lũ đểu cáng, chúng nó lải nhải về việc trái tim con gái lạnh lẽo đến đau khổ là bởi vì chúng nó không thật sự đặt chân đến Tà Năng, ở cái nơi mà niềm tự hào của đàn ông rút ngắn xuống còn 3cm thì có cái con khỉ mày sẽ quan tâm được người yêu cũ của mày ngủ ở nhà thằng nào.

Tuy nhiên Lộc vẫn tỉnh táo, đó là điều tốt quá ấy chứ, nó chịu trách nhiệm cho toàn bộ hình ảnh và video clip các bạn xem ở đây, tôi đồ rằng niềm tự hào đàn ông của nó dù nóng hay lạnh vẫn chỉ 3cm, và cho dù người yêu cũ của nó có ngủ với nhà thằng khác thì 3cm ấy vẫn chẳng mảy may quan tâm.

Anh Cường (trung chuyển) xuất hiện và giải cứu bọn tôi, cũng như phổ cập các kiến thức về cung đường. Như một định luật vùng đất đỏ, những người lái máy cày, xe hơi, xe máy, xe đạp và đi bộ đều là người tốt cả, người duy nhất tôi không tin đó là người lái máy bay (bà già).

Anh mở cửa cho chúng tôi vào nhà ngủ tá túc cho đến khi trời sáng để đi chợ và mua đồ.

Thẳng thắn mà nói, Đà Loan không phải là một cái chợ lý tưởng để mọi người có thể tìm thấy bất cứ cái gì mình muốn, nhưng với nhu cầu ăn uống và mua sắm cho chuyến trekking - đây là một lựa chọn tuyệt vời (hoặc là lựa chọn duy nhất).

Quẩn và Vỹ đã có một tranh cãi nhỏ nổ ra trước khi khoác balo lên vai. Vỹ muốn mượn chiếc khăn quàng màu đỏ của Quẩn để giữ ấm, Quẩn thì bảo rằng đéo đâu, đây là của người yêu tao tặng cho và mày lẽ ra nên soạn balo một cách kỹ lưỡng hơn chứ ? Vỹ thề rằng Quẩn là thằng chó keo kiệt khốn nạn, Quẩn thì lải nhải câu thơ:

**“Gỗ nào bằng gỗ căm xe, sịp nào bì được lọt khe của nàng”**

Chỉ có chúng tôi là không thể tin được tình bạn lại vỡ tan tành một cách như thế. Vỹ sau này có lẽ sẽ chẳng bán tí cổ phiếu nào cho Quẩn. Còn Quẩn thì vì mải mê đá ống đồng và rút ruột công trình nên đã không tính được đường dài ở tương lai thằng bạn học Ngoại Thương.

7h sáng, anh Cường ôm hôn và chúc tất cả mọi người lên đường bình an. Chúng tôi đeo balo lên vai cười tươi rói, trong khi Minh Đạt thì trốn vào lùm cây và tống nốt các sự lo lắng còn lại ra ngoài bằng đường ruột. **Chẳng một ai biết rằng 24 tiếng sắp tới chúng tôi sẽ bị hội đồng nhừ tử bởi mẹ thiên nhiên, cảm giác như vừa bị đấm vào mặt và sút lên hạ bộ cùng một lúc, nằm lăn lộn rên rỉ mà không biết nên ôm lấy chỗ nào.**

Đoạn đầu khá bằng phẳng vui vẻ. Chúng tôi trò chuyện cùng nhau về tác động của cocain lên Sigmund Freud, liệu chủ nghĩa khắc kỉ có thật sự liên quan đến tự kỉ và Vũ Trọng Phụng đã viết Làm Đĩ trong hoàn cảnh như thế nào ?

Thời gian càng kéo dài và độ dốc càng lên cao, với trọng lượng mỗi người ít nhất 7kg. Chúng tôi dần thấm mệt dưới cái nắng Tây Nguyên, chẳng ai muốn tỏ ra quan tâm tới bạn bè để xách giùm cái lều hay chai nước.

Dần dà mọi người đã thôi trò chuyện hăng say, tôi và Lộc bắt đầu ước được trở thành Freud để hít cocain công khai, phá tan đi sự mệt mỏi với danh nghĩa nghiên cứu khoa học mà không bị kì thị; Anh em Huy, Tuấn thì bắt đầu bước thẳng vào trạng thái tự kỉ mà đếu cần quan tâm đến khắc kỷ là cái ~~chó~~ chi, đồng thời Minh Đạt cũng gào to bản thân là một con đĩ còn thảm hại ~~hơn cả tác phẩm của Vũ Trọng Phụng. **(Nếu vị tác giả quá cố ấy đi chung chuyến Tà Năng với bạn tôi, có lẽ ông ấy sẽ muốn đặt tựa đề cho cuốn sách là “Làm Đạt” hơn**).~~

Nói thêm về cung đường Tà Năng, đây là một trong những đoạn trekking nổi tiếng không phải bàn cãi, nối liền 2 tỉnh Lâm Đồng và Bình Thuận, nếu đi vào mùa mưa, bạn sẽ lập tức choáng ngợp bởi độ xanh, mát lạnh và các con dốc uốn lượn như vòng eo em gái mười tám. Còn nếu đi vào mùa khô (mùa cỏ cháy) thì bạn sẽ đối mặt với vùng đất hoang vu cằn cỗi một cách quyến rũ, hệt như châu Phi mà Hemingway mô tả trong các chuyến đi săn. Và mùa nào thì cũng đáng để các bạn lăn lộn vui vẻ cả.

Chúng tôi ưỡn ngực và căng nọc cuốc bộ ròng rã 6 tiếng đồng hồ, vượt qua những cây thông cổ thụ đổ sạp, những con dốc dài lê thê và chào hỏi những người dân tộc đi làm rẫy. **Điện thoại chúng tôi tắt ngúm để chống lại sự công nghiệp hoá từ thành thị, đôi giày chúng tôi buộc chặt để nhắc nhở cuộc sống nơi Sài Gòn lỏng lẻo ra sao. Chúng tôi len lỏi dưới ánh mặt trời, dẫm lên hàng triệu bước chân Tây Nguyên đã khai phá vùng đất.**

Cuộc trekking thật sự rất mệt mỏi và có đôi lúc lũ bạn tôi phải đánh giá lại mức độ đàn ông của chính mình. Quẩn và Đạt phải chia nhau cùng xách cái lều một cách yếu đuối, phải nói thêm một trong hai đứa có HCĐ Taekwondo quốc gia, và đứa còn lại có thể chạy hùng hục 90p trên sân cỏ xanh (cỏ đen thì 9p) mà không hề hấn. Lộc thì không còn khả năng tập trung để quay phim chụp hình một cách cẩn thận, tôi đồ rằng sau khi ra clip, sẽ có rất nhiều tiếng bíp được thêm vào để át đi cái sự đê tiện mà con người xả ra khi kiệt sức.

1h chiều và cuối cùng cũng tới nóc nhà Tà Năng - nơi đẹp nhất và đáng cắm trại nhất cả chuyến hành trình, mọi người dừng lại làm bức ảnh vui vẻ, sau đó lựa một sườn dốc thấy chân trời, nằm ngủ mặc kệ cái nắng Tây Nguyên, mà thật ra ngay từ đầu mặt trời đã chẳng hề cháy bằng tuổi trẻ.

Công cuộc dựng lều cũng không dễ dàng khi chúng tôi phải chọn một địa điểm vừa bằng phẳng và ít gió, hầu như chỉ được chọn một trong hai, thế nên tôi và Vỹ miễn cưỡng rằng mọi người sẽ ngủ bên sườn hơi dốc, nhưng né được gió lạnh của núi rừng.

Quẩn và Duy chịu trách nhiệm đi phát quang cây cỏ và tìm củi, Huy và Đạt thay phiên nhau tìm cách dựng lều, còn Tuấn nằm xả lai ra sau khi chân có vẻ đã gặp vấn đề.

Lửa được nhóm lên trước khi mặt trời trốn sau dãy núi, chúng tôi lấy đa số đồ ăn ra mà chế biến bù cho việc đã bị hành hạ cả ngày hôm nay, đã có vài món ra đời và trở thành thương hiệu của Daboiz, không thể không nhắc đến “thịt nướng rựa” - cây rựa mà chúng tôi thay phiên nhau từ Huy tới Quẩn chặt cây cỏ cho tới hốt phân (không đùa) - chúng tôi khử khuẩn sơ sơ bằng cồn 90 độ và cứ thế quăng thịt lên miếng kim loại mà nướng mê say. Suy cho cùng thì bọn tôi đã ăn cứt cả ngày hôm nay rồi, thêm một tí vào cuối ngày không là vấn đề gì cả.

Đêm đó gió lạnh rít lên từng cơn, cảm tưởng sẽ kéo sập cả khu rừng lên lửa trại.

Đây là dịp lý tưởng để chúng tôi ngồi quây quần nói những chuyện tưởng chừng đã chết và chôn vùi hàng năm tháng, chúng tôi lúc này chợt thấy can đảm và yên tĩnh đến cùng cực, có lẽ là vì thật sự tin tưởng nhau và biết rằng các câu chuyện sẽ được trân trọng bất kể sự lố bịch.

Cầu mong thần lửa thổi đêm nay trải dài khắp Tây Nguyên và thần rừng sẽ ám những lời bộc bạch vào thân cây sắp vỡ vụn.

Chúng tôi đã sống và cười với nhau dưới những vì sao rất gần rất sáng.

“Chuyện gì xảy ra ở Tà Năng thì nằm lại ở Tà Năng” - tôi tôn trọng công thức này, nên xin phép không đi thêm quá sâu vào việc đã thú nhận với nhau những gì.

Sáng hôm sau, tỉnh dậy, uống cà phê và đốt tất cả những rác thải còn sót lại.

Chúng tôi hôn vùng đất và lại rời đi huy hoàng hơn lúc đến.

Quãng đường trở về giờ đã gần hơn sau khi balo được nhẹ đi và đôi chân cũng quen với các đồi dốc. Chúng tôi cũng có quay một đoạn quảng cáo nhỏ cho một nhãn hàng không tiện nêu tên, nếu chúng tôi nổi tiếng thì cứ thoải mái liên lạc nhé !

Kết thúc chuyến hành trình ở tiệm tạp hoá nhỏ đầu đoạn khởi hành, chúng tôi mệt nhoài và thở nằm vật ra bất cứ nơi nào có thể, uống ừng ực từng chai nước ngọt được đưa ra trong khi đợi anh Cường tới đón.

***Theo văn hoá Trung Đông, phủi sạch bụi bẩn và rửa chân trước khi bước vào nhà mang ý nghĩa rũ bỏ quá khứ và được tái sinh.***

***Nhưng đối với chúng tôi, chừng nào chân còn dính đất và chiếc áo còn bị thiêu đốt bởi nắng vàng thì lúc đấy chúng tôi mới thực sự sống, thực sự tái sinh.***

chú thích

“Cocain là kẻ phá tan lo lắng” - Freud

Các chi tiết "ôm", "hôn" trong đoạn chỉ mang tính chất lãng mạn, bởi tác giả đã đắm chìm 3 ngày 2 đêm trong văn học Pháp và giai điệu của bài "Òu Es Tu". Có thể hiểu "Ôm" là cú đấm hoặc tát (Cử chỉ thân mật), "Hôn" là đoạn bị làm cho lảo đảo và bị ngã cắm mặt xuống đất.
