module.exports = {
  purge: [
    './_includes/**/*.html',
    './_includes/**/*.svg',
    './_layouts/**/*.html',
    './_posts/*.html',
    './*.html',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        primary: '#F06A52',
        dark: '#0A3944',
        light: '#F9EDE1',
        lighter: '#FCF5EE',
        background: '#141F26',
        foreground: '#E4DACE',
        accent: '#A5F0B6'
      },
      fontFamily: {
        'sans': ['Space Grotesk', 'system-ui'],
        'display': ['Special Elite', 'system-ui'],
        'brand' : ['Special Elite'],
        'serif': ['EB Garamond'],
        'body': ['Cormorant Garamond', 'system-ui']
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/line-clamp'),
    require('@tailwindcss/aspect-ratio'),
  ]
}
